@extends('layouts.mainlayout')

@section('title', 'Booking')

@section('styles')
    <style>
        /* Pastikan navbar tetap di atas */
        nav {
        position: fixed;
        top: 0;
        width: 100%;
        z-index: 1000;
    }
        .content {
        padding-top: 70px;
    }
    </style>
@endsection

@section('content')
<div class="container">
    <h1>Book a Car</h1>
    <form action="{{ route('booking.store') }}" method="POST">
        @csrf
        <div class="form-group">
            <label for="car_id">Select Car</label>
            <select class="form-control my-3" id="car_id" name="car_id" required>
                @foreach($cars as $car)
                    <option value="{{ $car->id }}">{{ $car->brand }} - {{ $car->car_name }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group mb-4">
            <label for="return_date">Return Date</label>
            <input type="datetime-local" class="form-control" id="return_date" name="return_date" required>
        </div>
        <button type="submit" class="btn btn-primary">Book Now</button>
    </form>
</div>
@endsection

@section('sidebar_item')
    @include('partials.sidebar')
@endsection
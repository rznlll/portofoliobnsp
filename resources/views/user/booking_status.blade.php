@extends('layouts.mainlayout')

@section('title', 'Booking Status')

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
    <style>
        nav {
            position: fixed;
            top: 0;
            width: 100%;
            z-index: 1000;
        }
        .content {
            padding-top: 70px;
        }
        .blockquote-custom {
            position: relative;
            font-size: 1.1rem;
        }
        .blockquote-custom-icon {
            width: 50px;
            height: 50px;
            border-radius: 50%;
            display: flex;
            align-items: center;
            justify-content: center;
            position: absolute;
            top: -25px;
            left: 50px;
        }
    </style>

@section('content')
    <div class="container">
        @if(session('error'))
            <div class="alert alert-danger">{{ session('error') }}</div>
        @endif
        @if(session('success'))
            <div class="alert alert-success">{{ session('success') }}</div>
        @endif
        @if($rentLog)
            @if($rentLog->rent_status == 'in_process')
                <section class="py-5">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 mx-auto">
                                <!-- CUSTOM BLOCKQUOTE -->
                                <blockquote class="blockquote blockquote-custom bg-white p-5 shadow rounded">
                                    <div class="blockquote-custom-icon bg-info shadow-sm"><i class="fa fa-quote-left text-white"></i></div>
                                    <p class="mb-0 mt-2 font-italic">Your car booking status is being processed.</p>
                                    <footer class="blockquote-footer pt-4 mt-4 border-top">Copyright &copy; 
                                        <cite title="Source Title">Rent Car Brow.</cite>
                                    </footer>
                                </blockquote><!-- END -->
                            </div>
                        </div>
                    </div>
                </section>
            @elseif($rentLog->rent_status == 'approved')
                <section class="py-5">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 mx-auto">
                                <!-- CUSTOM BLOCKQUOTE -->
                                <blockquote class="blockquote blockquote-custom bg-white p-5 shadow rounded">
                                    <div class="blockquote-custom-icon bg-info shadow-sm"><i class="fa fa-quote-left text-white"></i></div>
                                    <p class="mb-0 mt-2 font-italic">Please come and pick up your car at jl. General Soedirman no.57 Kebayoran Baru Jakarta.</p>
                                    <footer class="blockquote-footer pt-4 mt-4 border-top">Copyright &copy; 
                                        <cite title="Source Title">Rent Car Brow.</cite>
                                    </footer>
                                </blockquote><!-- END -->
                            </div>
                        </div>
                    </div>
                </section>
            @elseif($rentLog->rent_status == 'rejected')
                <section class="py-5">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 mx-auto">
                                <!-- CUSTOM BLOCKQUOTE -->
                                <blockquote class="blockquote blockquote-custom bg-white p-5 shadow rounded">
                                    <div class="blockquote-custom-icon bg-info shadow-sm"><i class="fa fa-quote-left text-white"></i></div>
                                    <p class="mb-0 mt-2 font-italic">Your order was rejected by the admin.</p>
                                    <footer class="blockquote-footer pt-4 mt-4 border-top">Copyright &copy; 
                                        <cite title="Source Title">Rent Car Brow.</cite>
                                    </footer>
                                </blockquote><!-- END -->
                            </div>
                        </div>
                    </div>
                </section>
            @elseif($rentLog->rent_status == 'finished')
                <section class="py-5">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 mx-auto">
                                <!-- CUSTOM BLOCKQUOTE -->
                                <blockquote class="blockquote blockquote-custom bg-white p-5 shadow rounded">
                                    <div class="blockquote-custom-icon bg-info shadow-sm"><i class="fa fa-quote-left text-white"></i></div>
                                    <p class="mb-0 mt-2 font-italic">No orders yet.</p>
                                    <footer class="blockquote-footer pt-4 mt-4 border-top">Copyright &copy; 
                                        <cite title="Source Title">Rent Car Brow.</cite>
                                    </footer>
                                </blockquote><!-- END -->
                            </div>
                        </div>
                    </div>
                </section>
            @endif
        @else
            <div class="card">
                <div class="card-body">
                    No Orders Yet.
                </div>
            </div>
        @endif
    </div>
@endsection

@section('sidebar_item')
    @include('partials.sidebar')
@endsection

@section('scripts')
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
@endsection
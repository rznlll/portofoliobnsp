@extends('layouts.mainlayout')

@section('title', 'Car List')

    <style>
        /* Pastikan navbar tetap di atas */
        nav {
        position: fixed;
        top: 0;
        width: 100%;
        z-index: 1000;
    }
        .content {
        padding-top: 70px;
    }
    </style>

@section('content')
<body>
    <!-- Header-->
    <header class="bg-dark py-5">
        <div class="container px-4 px-lg-5 my-5">
            <div class="text-center text-white">
                <h1 class="display-4 fw-bolder">Rent Car Brow</h1>
                <p class="lead fw-normal text-white-50 mb-0">- Enhance your journey with Rent Car Brow, the ultimate car rental solution! -</p>
            </div>
        </div>
    </header>
    <!-- Section-->

    <form action="{{ route('carlist.index') }}" method="get">
        <div class="row my-4">
            <div class="col-12 col-sm-6 mb-3">
                <select name="category" id="category" class="form-control">
                    <option value="">
                        Select Car Category
                    </option>
                    @foreach ($categories as $item)
                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-12 col-sm-6">
                <div class="input-group mb-3">
                    <input type="text" class="form-control" placeholder="Find some cars" aria-label="Recipient's username" aria-describedby="basic-addon2">
                    <button class="btn btn-primary">Search</button>
                  </div>
            </div>
        </div>
    </form>

    <section class="py-5">
        <div class="my-5">
            <div class="row">
                @foreach ($cars as $item)
                <div class="col-lg-3 col-md-4 col-sm-6 mb-3">
                    <div class="card h-100">
                        <img src="{{ $item->cover != null ? asset('storage/cover/'.$item->cover) : asset('images/default.jpeg') }}" 
                        alt="" class="card-img-top" draggable="false">                   
                        <div class="card-body">
                            <h5 class="card-title">{{ $item->car_code }}</h5>
                            <p class="card-text">{{ $item->brand }}</p>
                            <p class="card-text">{{ $item->car_name }}</p>
                            <p class="card-text text-end fw-bold
                            {{ $item->status == 'available' ? 'text-success' : 'text-danger' }}">
                            {{ $item->status }}</p>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
    <!-- Footer-->
    <footer class="py-5 bg-dark">
        <div class="container"><p class="m-0 text-center text-white">Copyright &copy; Rent Car Brow.</p></div>
    </footer>
</body>
@endsection

@section('sidebar_item')
    @include('partials.sidebar')
@endsection
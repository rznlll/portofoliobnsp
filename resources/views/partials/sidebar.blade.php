<style>
    .nav-link:hover {
        color: #ffcc00; 
        background-color: #333333;  
        text-decoration: none;
    }

    .active {
        color: #ffcc00; 
        background-color: #333333; 
        text-decoration: none;
    }
</style>
@if (Auth::user()->role_id == 1)
    <li class="nav-item">
        <a href="{{ route('admin.dashboard') }}" 
            class="nav-link text-light {{ request()->is('admin-dashboard') ? 'active' : '' }}">
            <span style="margin-right: 3px"><i class="bi bi-house-door"></i></span>
            Beranda
        </a>
    </li>
    <li class="nav-item">
        <a href="{{ route('admin.profile') }}"
            class="nav-link text-light {{ request()->is('admin-profile') ? 'active' : '' }}">
            <span style="margin-right: 3px"><i class="bi bi-person"></i></span>
            Profile
        </a>
    </li>
    <li class="nav-item">
        <a href="{{ route('users.index') }}"
            class="nav-link text-light {{ (Route::currentRouteName() == 'users.index' || Route::currentRouteName() == 'users.create' || Route::currentRouteName() == 'users.edit') ? 'active' : '' }}">
            <span style="margin-right: 3px"><i class="bi bi-people"></i></span>
            Users
        </a>
    </li>
    <li class="nav-item">
        <a href="{{ route('car.index') }}"
            class="nav-link text-light {{ (Route::currentRouteName() == 'car.index' || Route::currentRouteName() == 'car.create' || Route::currentRouteName() == 'car.edit') ? 'active' : '' }}">
            <span style="margin-right: 3px"><i class="bi bi-car-front"></i></span>
            Car
        </a>
    </li>    
    <li class="nav-item">
        <a href="{{ route('category.index') }}"
            class="nav-link text-light {{ (Route::currentRouteName() == 'category.index' || Route::currentRouteName() == 'category.create' || Route::currentRouteName() == 'category.edit') ? 'active' : '' }}">
            <span style="margin-right: 3px"><i class="bi bi-ev-front"></i></span>
            Categories
        </a>
    </li>    
    <li class="nav-item">
        <a href="{{ route('rent-data.index') }}"
            class="nav-link text-light {{ (Route::currentRouteName() == 'rent-data.index' || Route::currentRouteName() == 'rent-data.edit') ? 'active' : '' }}">
            <span style="margin-right: 3px;"><i class="bi bi-journal-text"></i></span>
            Rent Data
        </a>
    </li>
@else
    <li class="nav-item">
        <a href="{{ route('user.dashboard') }}"
            class="nav-link text-light {{ request()->is('user-dashboard') ? 'active' : '' }}">
            <span style="margin-right: 3px"><i class="bi bi-house-door"></i></span>
            Beranda
        </a>
    </li>
    <li class="nav-item">
        <a href="{{ route('user.profile') }}"
            class="nav-link text-light {{ request()->is('user-profile') ? 'active' : '' }}">
            <span style="margin-right: 3px"><i class="bi bi-person"></i></span>
            Profile
        </a>
    </li>
    <li class="nav-item">
        <a href="{{ route('carlist.index') }}"
            class="nav-link text-light {{ request()->is('car-list') ? 'active' : '' }}">
            <span style="margin-right: 3px"><i class="bi bi-car-front"></i></span>
            Car List
        </a>
    </li>
    <li class="nav-item">
        <a href="{{ route('booking.create') }}" 
            class="nav-link text-light {{ request()->is('booking') ? 'active' : '' }}">
            <span style="margin-right: 3px"><i class="bi bi-card-list"></i></span>
            Booking a Car
        </a>
    </li>
    <li class="nav-item">
        <a href="{{ route('booking.status') }}" 
            class="nav-link text-light {{ request()->is('booking/status') ? 'active' : '' }}">
            <span style="margin-right: 3px"><i class="bi bi-journal-text"></i></span>
            Booking Status
        </a>
    </li>    
@endif
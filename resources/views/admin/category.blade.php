@extends('layouts.mainlayout')

@section('title', 'Category')

<style>
        /* Pastikan navbar tetap di atas */
        nav {
        position: fixed;
        top: 0;
        width: 100%;
        z-index: 1000;
    }
        .content {
        padding-top: 70px;
    }
</style>

@section('content')
    <h1>
        Category List
    </h1>
    @if(session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @elseif(session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
    @endif

    <div class="mt-5 d-flex justify-content-end">
        <a href="{{ route('category.create') }}" class="btn btn-primary">
            <i class="bi bi-plus"></i> Add Category
        </a>
    </div>

    <div class="my-5">
        <table id="table-category" class="table table-striped" style="width:100%">
            <thead>
                <tr>
                    <th class="text-center px-3">No</th>
                    <th class="px-3">Car Category</th>
                    <th class="text-center px-3">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($categories as $category)
                    <tr>
                        <td class="text-center px-3">{{ $loop->iteration }}</td>
                        <td class="px-3">{{ $category->name }}</td>
                        <td class="text-center px-3">
                            <form action="{{ route('category.edit', ['category' => $category->id]) }}" method="GET" class="d-inline">
                                @csrf
                                @method('GET')
                                <button type="submit" class="btn btn-warning btn-sm">
                                    <i class="bi bi-pencil-square"></i> Edit
                                </button>
                            </form>
                            <form action="{{ route('category.destroy', $category->id) }}" method="POST" id="delete-form-{{ $category->id }}" class="d-inline">
                                @csrf
                                @method('DELETE')
                                <button type="button" name="hapus" class="btn btn-danger btn-sm" onclick="confirmDelete({{ $category->id }})">
                                    <i class="bi bi-trash3"></i> Hapus
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection

@push('css')
    <link rel="stylesheet" href="//cdn.datatables.net/2.0.8/css/dataTables.dataTables.min.css">
@endpush

@push('js')
    <script src="//cdn.datatables.net/2.0.8/js/dataTables.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#table-category').DataTable({
                scrollCollapse: true,
                ordering: false
            });

            @if(session('success'))
                swal("Good job!", "{{ session('success') }}", "success");
            @endif
        });

        function confirmDelete(categoryId) {
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this category!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    document.getElementById('delete-form-' + categoryId).submit();
                } else {
                    swal("This category data is safe!");
                }
            });
        }
    </script>
@endpush

@section('sidebar_item')
    @include('partials.sidebar')
@endsection
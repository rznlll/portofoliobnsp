@extends('layouts.mainlayout')

@section('title', 'Rent Data')

<style>
    /* Pastikan navbar tetap di atas */
    nav {
        position: fixed;
        top: 0;
        width: 100%;
        z-index: 1000;
    }
    .content {
        padding-top: 70px;
    }
</style>

@section('content')
    <h1 class="mb-5">Rent Data</h1>
    @if(session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @elseif(session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
    @endif

    <table id="table-rentdata" class="table table-striped" style="width:100%">
        <thead>
            <tr>
                <th class="text-center">No</th>
                <th class="text-center">Renters</th>
                <th class="text-center">Car Name</th>
                <th class="text-center">Rent Date</th>
                <th class="text-center">Return Date</th>
                <th class="text-center">Status</th>
                <th class="text-center">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($rentLogs as $index => $rentLog)
                <tr>
                    <td class="text-center">{{ $index + 1 }}</td>
                    <td class="text-center">{{ $rentLog->user->username }}</td>
                    <td class="text-center">{{ $rentLog->car->car_name }}</td>
                    <td class="text-center">{{ $rentLog->rent_date }}</td>
                    <td class="text-center">{{ $rentLog->return_date }}</td>
                    <td class="text-center">{{ ucfirst($rentLog->rent_status) }}</td>
                    <td class="text-center">
                        <a href="{{ route('rent-data.edit', $rentLog->id) }}" class="btn btn-warning btn-sm">
                            <i class="bi bi-pencil-square"></i> Edit
                        </a>
                        <form id="delete-form-{{ $rentLog->id }}" action="{{ route('rent-data.destroy', $rentLog->id) }}" method="POST" style="display:inline-block;">
                            @csrf
                            @method('DELETE')
                            <button type="button" class="btn btn-danger btn-sm" onclick="confirmDelete({{ $rentLog->id }})">
                                <i class="bi bi-trash3"></i> Hapus
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection

@push('css')
    <link rel="stylesheet" href="//cdn.datatables.net/2.0.8/css/dataTables.dataTables.min.css">
@endpush

@push('js')
    <script src="//cdn.datatables.net/2.0.8/js/dataTables.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#table-rentdata').DataTable({
                ordering: false,
                responsive: true
            });

            @if(session('success'))
                swal("Good job!", "{{ session('success') }}", "success");
            @endif
        });
    
        function confirmDelete(rentLogId) {
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this rent data!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    document.getElementById('delete-form-' + rentLogId).submit();
                } else {
                    swal("This rent data is safe!");
                }
            });
        }
    </script>
@endpush

@section('sidebar_item')
    @include('partials.sidebar')
@endsection
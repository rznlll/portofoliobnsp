@extends('layouts.mainlayout')

@section('title', 'Create Car')

@section('content')
    <h1>Create Car</h1>

    @if($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('car.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group mt-3">
            <label for="car_code">Car Code</label>
            <input type="text" name="car_code" class="form-control" id="car_code" value="{{ old('car_code') }}" required>
        </div>
        <div class="form-group mt-3">
            <label for="merek">Brand</label>
            <input type="text" name="brand" class="form-control" id="brand" value="{{ old('brand') }}" required>
        </div>
        <div class="form-group mt-3">
            <label for="car_name">Car Name</label>
            <input type="text" name="car_name" class="form-control" id="car_name" value="{{ old('car_name') }}" required>
        </div>
        <div class="form-group mt-3">
            <label for="status">Status</label>
            <select name="status" class="form-control" id="status" required>
                <option value="available" {{ old('status') == 'available' ? 'selected' : '' }}>Available</option>
                <option value="unavailable" {{ old('status') == 'unavailable' ? 'selected' : '' }}>Unavailable</option>
            </select>
        </div>
        <div class="mt-3">
            <label for="image" class="form-label">Upload Car Image</label>
            <input type="file" name="image" class="form-control">
        </div>   
        <div class="mt-3">
            <label for="category" class="form-label">Category</label>
            <select name="categories" id="category" class="form-control">
                <option value="">
                    @foreach ($categories as $item)
                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                    @endforeach
                </option>
            </select>
        </div> 
        <button type="submit" class="btn btn-primary mt-4">Create Car</button>
    </form>
@endsection

@section('sidebar_item')
    @include('partials.sidebar')
@endsection

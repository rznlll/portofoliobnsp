@extends('layouts.mainlayout')

@section('title', 'Dashboard')

<style>
    .card-data {
        border: solid 1px;
        padding: 10px 30px;
        border-radius: 5px;
        background-color: orange;
        color: #fff;
    }

    .card-data i {
        font-size: 50px;
    }

    .card-desc {
        font-size: 15px;
        font-weight: 600;
    }

    .card-count {
        font-size: 15px;
        font-weight: 600;
    }

    /* Pastikan navbar tetap di atas */
    nav {
        position: fixed;
        top: 0;
        width: 100%;
        z-index: 1000;
    }

    .content {
        padding-top: 70px;
    }
</style>

@section('content')
    <div class="row mt-3">
        <div class="col-lg-4 mt-2">
            <a href="{{ route('users.index') }}" class="text-decoration-none">
                <div class="card-data">
                    <div class="row">
                        <div class="col-6">
                            <i class="bi bi-people"></i>
                        </div>
                        <div class="col-6 d-flex flex-column justify-content-center">
                            <div class="card-desc">Users</div>
                            <div class="card-count">{{ $user_count }}</div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-lg-4 mt-2">
            <a href="{{ route('car.index') }}" class="text-decoration-none">
                <div class="card-data">
                    <div class="row">
                        <div class="col-6">
                            <i class="bi bi-car-front"></i>
                        </div>
                        <div class="col-6 d-flex flex-column justify-content-center">
                            <div class="card-desc">Car</div>
                            <div class="card-count">{{ $car_count }}</div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-lg-4 mt-2">
            <a href="{{ route('category.index') }}" class="text-decoration-none">
                <div class="card-data">
                    <div class="row">
                        <div class="col-6">
                            <i class="bi bi-ev-front"></i>
                        </div>
                        <div class="col-6 d-flex flex-column justify-content-center">
                            <div class="card-desc">Category</div>
                            <div class="card-count">{{ $category_count }}</div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="mt-5">
        <h1>Rent Logs</h1>
        <table id="table-listrent" class="table table-striped mt-4" style="width:100%">
            <thead>
                <tr>
                    <th class="text-center">No</th>
                    <th class="text-center">Renters</th>
                    <th class="text-center">Car Name</th>
                    <th class="text-center">Rent Date</th>
                    <th class="text-center">Return Date</th>
                    <th class="text-center">Status</th>
                </tr>
            </thead>
            <tbody>
                @foreach($rent_logs as $index => $rentLog)
                    <tr>
                        <td class="text-center">{{ $index + 1 }}</td>
                        <td class="text-center">{{ $rentLog->user->username }}</td>
                        <td class="text-center">{{ $rentLog->car->car_name }}</td>
                        <td class="text-center">{{ $rentLog->rent_date }}</td>
                        <td class="text-center">{{ $rentLog->return_date }}</td>
                        <td class="text-center">{{ ucfirst($rentLog->rent_status) }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection

@push('css')
    <link rel="stylesheet" href="//cdn.datatables.net/2.0.8/css/dataTables.dataTables.min.css">
@endpush

@push('js')
    <script src="//cdn.datatables.net/2.0.8/js/dataTables.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#table-listrent').DataTable({
                scrollY: '50vh',
                scrollCollapse: true,
                ordering: false
            });
        });
    </script>
@endpush

@section('sidebar_item')
    @include('partials.sidebar')
@endsection

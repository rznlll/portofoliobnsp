@extends('layouts.mainlayout')

@section('title', 'Edit Car')

@section('content')
    <h1>Edit Car</h1>

    @if($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('car.update', $car->id) }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="form-group mt-3">
            <label for="car_code">Car Code</label>
            <input type="text" name="car_code" class="form-control" id="car_code" value="{{ old('car_code', $car->car_code) }}" required>
        </div>
        <div class="form-group mt-3">
            <label for="brand">Brand</label>
            <input type="text" name="brand" class="form-control" id="brand" value="{{ old('brand', $car->brand) }}" required>
        </div>
        <div class="form-group mt-3">
            <label for="car_name">Car Name</label>
            <input type="text" name="car_name" class="form-control" id="car_name" value="{{ old('car_name', $car->car_name) }}" required>
        </div>
        <div class="form-group mt-3">
            <label for="status">Status</label>
            <select name="status" class="form-control" id="status" required>
                <option value="available" {{ old('status', $car->status) == 'available' ? 'selected' : '' }}>Available</option>
                <option value="unavailable" {{ old('status', $car->status) == 'unavailable' ? 'selected' : '' }}>Unavailable</option>
            </select>
        </div>
        <div class="form-group mt-3">
            <label for="categories">Categories</label>
            <select name="categories[]" class="form-control" id="categories" multiple>
                @foreach($categories as $category)
                    <option value="{{ $category->id }}" 
                        @if($car->categories->contains($category->id)) selected @endif>
                        {{ $category->name }}
                    </option>
                @endforeach
            </select>
        </div>
        <div class="form-group mt-3">
            <label for="image">Upload Car Image</label>
            <input type="file" name="image" class="form-control">
        </div>
        <button type="submit" class="btn btn-primary mt-4">Update Car</button>
    </form>
@endsection

@section('sidebar_item')
    @include('partials.sidebar')
@endsection

@extends('layouts.mainlayout')

@section('title', 'Add Category')

@section('content')
<div class="container mt-5">
    <h1>Add Category</h1>

    @if($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('category.store') }}" method="POST">
        @csrf
        <div class="mb-3 mt-3">
            <label for="name" class="form-label">Category Name</label>
            <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" required>
        </div>
        <button type="submit" class="btn btn-primary">Add Category</button>
    </form>
</div>
@endsection

@push('js')
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>
        $(document).ready(function() {
            @if(session('success'))
                swal("Good job!", "{{ session('success') }}", "success");
            @endif
        });
    </script>
@endpush

@section('sidebar_item')
    @include('partials.sidebar')
@endsection
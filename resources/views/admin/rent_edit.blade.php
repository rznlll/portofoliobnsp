@extends('layouts.mainlayout')

@section('title', 'Edit Rent Data')

@section('content')
<div class="container my-5">
    <form action="{{ route('rent-data.update', $rentLog->id) }}" method="POST">
        @csrf
        @method('PUT')
        
        <div class="form-group mt-3">
            <label for="username">Renters</label>
            <input type="text" class="form-control" id="username" value="{{ $rentLog->user->username }}" readonly>
        </div>
        
        <div class="form-group mt-3">
            <label for="car_name">Car Name</label>
            <input type="text" class="form-control" id="car_name" value="{{ $rentLog->car->car_name }}" readonly>
        </div>
        
        <div class="form-group mt-3">
            <label for="rent_date">Rent Date</label>
            <input type="date" class="form-control" id="rent_date" name="rent_date" value="{{ $rentLog->rent_date }}" required>
        </div>
        
        <div class="form-group mt-3">
            <label for="return_date">Return Date</label>
            <input type="date" class="form-control" id="return_date" name="return_date" value="{{ $rentLog->return_date }}" required>
        </div>
        
        <div class="form-group mt-3">
            <label for="rent_status">Status</label>
            <select class="form-control" id="rent_status" name="rent_status" required>
                <option value="in_process" {{ $rentLog->rent_status == 'in_process' ? 'selected' : '' }}>In Process</option>
                <option value="approved" {{ $rentLog->rent_status == 'approved' ? 'selected' : '' }}>Approved</option>
                <option value="rejected" {{ $rentLog->rent_status == 'rejected' ? 'selected' : '' }}>Rejected</option>
                <option value="finished" {{ $rentLog->rent_status == 'finished' ? 'selected' : '' }}>Finished</option>
            </select>
        </div>

        <button type="submit" class="btn btn-primary mt-4">Update</button>
    </form>
</div>
@endsection

@section('sidebar_item')
    @include('partials.sidebar')
@endsection
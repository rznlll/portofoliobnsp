@extends('layouts.mainlayout')

@section('title', 'Users Management')

<style>
    /* Pastikan navbar tetap di atas */
    nav {
    position: fixed;
    top: 0;
    width: 100%;
    z-index: 1000;
}
    .content {
    padding-top: 70px;
}
table {
        width: 100%;
        border-collapse: collapse;
    }
    th, td {
        padding: 8px 12px;
        text-align: left;
        border: 1px solid #ddd;
    }
    th {
        background-color: #f2f2f2;
        text-align: center;
    }
    .text-center {
        text-align: center;
    }
    .text-right {
        text-align: right;
    }
</style>

@section('content')
    <h1>Users Management</h1>
    @if(session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @elseif(session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
    @endif

    <div class="mt-5 d-flex justify-content-end">
        <a href="{{ route('users.create') }}" class="btn btn-primary">
            <i class="bi bi-plus"></i> Add User
        </a>
    </div>

    <div class="my-5">
        <table id="table-user" class="table table-striped" style="width:100%">
            <thead>
                <tr>
                    <th class="text-center">No</th>
                    <th>Username</th>
                    <th class="text-center">Phone</th>
                    <th>Address</th>
                    <th class="text-center">Status</th>
                    <th class="text-center">Role</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($users as $user)
                    <tr>
                        <td class="text-center">{{ $loop->iteration }}</td>
                        <td>{{ $user->username }}</td>
                        <td class="text-center">{{ $user->phone }}</td>
                        <td>{{ $user->address }}</td>
                        <td class="text-center">{{ $user->status }}</td>
                        <td class="text-center">{{ $user->role_id == 1 ? 'Admin' : 'Client' }}</td>
                        <td class="text-center">
                            <a href="{{ route('users.edit', $user->id) }}" class="btn btn-warning btn-sm">
                                <i class="bi bi-pencil-square"></i> Edit
                            </a>
                            <form id="delete-form-{{ $user->id }}" action="{{ route('users.destroy', $user->id) }}" method="POST" style="display:inline-block;">
                                @csrf
                                @method('DELETE')
                                <button type="button" class="btn btn-danger btn-sm" onclick="confirmDelete({{ $user->id }})">
                                    <i class="bi bi-trash3"></i> Hapus
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

@endsection

@push('css')
    <link rel="stylesheet" href="//cdn.datatables.net/2.0.8/css/dataTables.dataTables.min.css">
@endpush

@push('js')
    <script src="//cdn.datatables.net/2.0.8/js/dataTables.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#table-user').DataTable({
                ordering: true,
                responsive: true
            });

            @if(session('success'))
                swal("Good job!", "{{ session('success') }}", "success");
            @endif
        });

        function confirmDelete(userId) {
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this user!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    document.getElementById('delete-form-' + userId).submit();
                } else {
                    swal("This user data is safe!");
                }
            });
        }
    </script>
@endpush

@section('sidebar_item')
    @include('partials.sidebar')
@endsection
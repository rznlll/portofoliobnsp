@extends('layouts.mainlayout')

@section('title', 'Cars Management')

<style>
    /* Pastikan navbar tetap di atas */
    nav {
        position: fixed;
        top: 0;
        width: 100%;
        z-index: 1000;
    }
    .content {
        padding-top: 70px;
    }
    /* Penataan tabel */
    table {
        width: 100%;
        border-collapse: collapse;
    }
    th, td {
        padding: 8px 12px;
        text-align: left;
        border: 1px solid #ddd;
    }
    th {
        background-color: #f2f2f2;
        text-align: center;
    }
    .text-center {
        text-align: center;
    }
    .text-right {
        text-align: right;
    }
</style>

@section('content')
    <h1>Cars Management</h1>
    @if(session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @elseif(session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
    @endif

    <div class="mt-5 d-flex justify-content-end">
        <a href="{{ route('car.create') }}" class="btn btn-primary">
            <i class="bi bi-plus"></i> Add Car
        </a>
    </div>

    <div class="my-5">
        <table id="table-car" class="table table-striped">
            <thead>
                <tr>
                    <th class="text-center">No</th>
                    <th class="text-center">Car Code</th>
                    <th class="text-center">Brand</th>
                    <th class="text-center">Car Name</th>
                    <th class="text-center">Category</th>
                    <th class="text-center">Status</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($cars as $car)
                    <tr>
                        <td class="text-center">{{ $loop->iteration }}</td>
                        <td class="text-center">{{ $car->car_code }}</td>
                        <td class="text-center">{{ $car->brand }}</td>
                        <td class="text-center">{{ $car->car_name }}</td>
                        <td class="text-center">
                            @foreach ($car->categories as $item)
                                {{ $item->name }}
                            @endforeach
                        </td>
                        <td class="text-center">{{ ucfirst($car->status) }}</td>
                        <td class="text-center">
                            <a href="{{ route('car.edit', $car->id) }}" class="btn btn-warning btn-sm">
                                <i class="bi bi-pencil-square"></i> Edit
                            </a>
                            <form id="delete-form-{{ $car->id }}" action="{{ route('car.destroy', $car->id) }}" method="POST" style="display:inline-block;">
                                @csrf
                                @method('DELETE')
                                <button type="button" class="btn btn-danger btn-sm" onclick="confirmDelete({{ $car->id }})">
                                    <i class="bi bi-trash3"></i> Hapus
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

@endsection

@push('css')
    <link rel="stylesheet" href="//cdn.datatables.net/2.0.8/css/dataTables.dataTables.min.css">
@endpush

@push('js')
    <script src="//cdn.datatables.net/2.0.8/js/dataTables.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#table-car').DataTable({
                ordering: true,
                responsive: true
            });

            @if(session('success'))
                swal("Good job!", "{{ session('success') }}", "success");
            @endif
        });

        function confirmDelete(carId) {
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this car!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    document.getElementById('delete-form-' + carId).submit();
                } else {
                    swal("This car data is safe!");
                }
            });
        }
    </script>
@endpush

@section('sidebar_item')
    @include('partials.sidebar')
@endsection

<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CarController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\BookingController;
use App\Http\Controllers\CarListController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\RentDataController;
use App\Http\Controllers\UserManagementController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


Route::get('/', function () {
    return view('landing');
});



// LOG IN
Route::get('/login', [AuthController::class, 'login'])->name('login');
Route::post('/login', [AuthController::class, 'prosesLogin']);

// SIGN UP
Route::get('/register', [AuthController::class, 'register']);
Route::post('/register', [AuthController::class, 'prosesRegist']);

// LOG OUT
Route::post('/logout', [AuthController::class, 'logout'])->name('logout');


// ADMIN
Route::middleware(['auth', 'only.admin'])->group(function () {

    // ADMIN DASHBOARD
    Route::get('/admin-dashboard', [AdminController::class, 'index'])->name('admin.dashboard');

    // ADMIN PROFILE
    Route::get('/admin-profile', [AdminController::class, 'adminProfile'])->name('admin.profile');
    
    // ADMIN RENT DATA
    Route::get('/admin-rent-data', [RentDataController::class, 'index'])->name('rent-data.index');
    Route::get('/rent-data/{rentLog}/edit', [RentDataController::class, 'edit'])->name('rent-data.edit');
    Route::put('/rent-data/{rentLog}', [RentDataController::class, 'update'])->name('rent-data.update');
    Route::delete('/rent-data/{rentLog}', [RentDataController::class, 'destroy'])->name('rent-data.destroy');

    // USER MANAGEMENT
    Route::get('/admin-users', [UserManagementController::class, 'index'])->name('users.index');
    Route::get('/users/create', [UserManagementController::class, 'create'])->name('users.create');
    Route::post('/users', [UserManagementController::class, 'store'])->name('users.store');
    Route::get('/users/{user}/edit', [UserManagementController::class, 'edit'])->name('users.edit');
    Route::put('/users/{user}', [UserManagementController::class, 'update'])->name('users.update');
    Route::delete('/users/{user}', [UserManagementController::class, 'destroy'])->name('users.destroy');

    // CAR
    Route::get('/admin-car', [CarController::class, 'index'])->name('car.index');
    Route::get('/admin-car/create', [CarController::class, 'create'])->name('car.create');
    Route::post('/admin-car', [CarController::class, 'store'])->name('car.store');
    Route::get('/admin-car/{car}/edit', [CarController::class, 'edit'])->name('car.edit');
    Route::put('/admin-car/{car}', [CarController::class, 'update'])->name('car.update');
    Route::delete('/admin-car/{car}', [CarController::class, 'destroy'])->name('car.destroy');

    // CATEGORY
    Route::get('/admin-category', [CategoryController::class, 'index'])->name('category.index');
    Route::get('/categories/create', [CategoryController::class, 'create'])->name('category.create');
    Route::post('/categories', [CategoryController::class, 'store'])->name('category.store');
    Route::get('categories/{category}/edit', [CategoryController::class, 'edit'])->name('category.edit');
    Route::put('categories/{category}', [CategoryController::class, 'update'])->name('category.update');
    Route::delete('/categories/{id}', [CategoryController::class, 'destroy'])->name('category.destroy');

    Route::get('/logout', [AuthController::class, 'logout'])->name('logout');
});

// USER
Route::middleware(['auth', 'only.client'])->group(function () {

    // DASHBOARD USER
    Route::get('/user-dashboard', [UserController::class, 'index'])->name('user.dashboard');

    // PROFILE USER
    Route::get('/user-profile', [UserController::class, 'userProfile'])->name('user.profile');

    // LIST CAR
    Route::get('/car-list', [CarListController::class, 'index'])->name('carlist.index');

    // BOOKING CAR
    Route::get('/booking', [BookingController::class, 'create'])->name('booking.create');
    Route::post('/booking', [BookingController::class, 'store'])->name('booking.store');
    Route::get('/booking/status', [BookingController::class, 'status'])->name('booking.status');

    Route::get('/logout', [AuthController::class, 'logout']);
});

<?php

namespace App\Models;

use App\Models\Category;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Car extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'car_code',
        'brand',
        'car_name',
        'status',
        'cover',
    ];

    /**
     * Define a many-to-many relationship with the Category model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(Category::class, 'car_category', 'car_id', 'category_id');
    }

    // public function getCoverUrlAttribute()
    // {
    //     return $this->cover ? asset('storage/cover/'.$this->cover) : null;
    // }
}

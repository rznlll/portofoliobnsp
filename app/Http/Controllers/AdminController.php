<?php

namespace App\Http\Controllers;

use App\Models\Car;
use App\Models\User;
use App\Models\Category;
use App\Models\RentLogs;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index() 
    {
    $carCount = Car::count();
    $categoryCount = Category::count();
    $userCount = User::count();
    $rent_logs = RentLogs::with('user', 'car')->get();

    return view('admin.dashboard', [
        'user_count' => $userCount,
        'car_count' => $carCount,
        'category_count' => $categoryCount,
        'rent_logs' => $rent_logs
    ]);
    }

    public function adminProfile()
    {
        return view('admin.profile');
    }
}

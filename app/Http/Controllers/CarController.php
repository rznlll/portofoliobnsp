<?php

namespace App\Http\Controllers;

use App\Models\Car;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class CarController extends Controller
{
    public function index ()
    {
        $cars = Car::all();
        return view('admin.car', compact('cars'));
    }

    public function create()
    {
        $categories = Category::all();
        return view('admin.car_create', ['categories' => $categories]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'car_code' => 'required|unique:cars|max:255',
            'brand' => 'required|string|max:255',
            'car_name' => 'required|string|max:255',
            'status' => 'required|string|in:available,unavailable',
        ]);
    
        // Inisialisasi nama file baru
        $newName = '';

        if ($request->file('image')) {
            $file = $request->file('image');
            // Dapatkan ekstensi file
            $extension = $file->getClientOriginalExtension();
            $carName = str_replace(' ', '_', $request->car_name);
            // Buat nama file baru menggunakan nama mobil dan timestamp
            $newName =  $carName . '_' .  Carbon::now()->format('YmdHis') . '.' . $extension;
            // Simpan file ke disk 'public' di folder 'cover'
            $path = $file->storeAs('cover', $newName, 'public');
        }
    
        $request['cover'] = $newName;

        $car = Car::create($request->all());
        $car->categories()->sync($request->categories);
    
        return redirect()->route('car.index')->with('success', 'Car added successfully.');
    }
    
    public function edit($id)
    {
        $car = Car::with('categories')->findOrFail($id);
        $categories = Category::all();
        return view('admin.car_edit', compact('car', 'categories'));
    }

    public function update(Request $request, $id)
    {
        // Validasi input termasuk validasi gambar
        $request->validate([
            'car_code' => 'required|max:255|unique:cars,car_code,' . $id,
            'brand' => 'required|string|max:255',
            'car_name' => 'required|string|max:255',
            'status' => 'required|string|in:available,unavailable',
        ]);
    
        // Temukan data mobil berdasarkan ID
        $car = Car::findOrFail($id);

        // Ambil semua data request kecuali 'image'
        $data = $request->except('image');

        // Proses upload gambar jika ada file yang diupload
        if ($request->file('image')) {
            // Dapatkan file yang diunggah
            $file = $request->file('image');
            
            // Dapatkan ekstensi file
            $extension = $file->getClientOriginalExtension();
            $carName = str_replace(' ', '_', $request->car_name);
            
            // Buat nama file baru menggunakan nama mobil dan timestamp
            $newName = $carName . '_' . Carbon::now()->format('YmdHis') . '.' . $extension;
            
            // Simpan gambar baru ke folder 'cover' di disk 'public'
            $path = $file->storeAs('cover', $newName, 'public');

            // Jika gambar lama ada, hapus gambar lama dari storage
            if ($car->cover) {
                Storage::disk('public')->delete('cover/' . $car->cover);
            }

            // Tambahkan nama file baru ke data yang akan diupdate
            $data['cover'] = $newName;
        }

        // Update data mobil dengan data yang telah diubah
        $car->update($data);
    
        // Sinkronisasi kategori jika ada
        if ($request->has('categories')) {
            $car->categories()->sync($request->categories);
        }
    
        // Redirect ke halaman index dengan pesan sukses
        return redirect()->route('car.index')->with('success', 'Car updated successfully.');
    }

    public function destroy(Car $car)
    {
        // Hapus gambar jika ada
        if ($car->cover) {
            Storage::disk('public')->delete('cover/' . $car->cover);
        }
    
        // Hapus data mobil
        $car->delete();
    
        return redirect()->route('car.index')->with('success', 'Car deleted successfully.');
    }
}

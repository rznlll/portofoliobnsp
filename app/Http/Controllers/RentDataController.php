<?php

namespace App\Http\Controllers;

use App\Models\RentLogs;
use App\Models\User;
use App\Models\Car;
use Illuminate\Http\Request;

class RentDataController extends Controller
{
    public function index()
    {
        $rentLogs = RentLogs::with('user', 'car')->get();
        return view('admin.rent_data', compact('rentLogs'));
    }

    public function edit(RentLogs $rentLog)
    {
        $rentLog->load('user', 'car');
        return view('admin.rent_edit', compact('rentLog'));
    }

    public function update(Request $request, RentLogs $rentLog)
    {
        $request->validate([
            'rent_date' => 'required|date',
            'return_date' => 'required|date|after:rent_date',
            'rent_status' => 'required|in:rejected,in_process,approved,finished'
        ]);

        $rentLog->update([
            'rent_date' => $request->rent_date,
            'return_date' => $request->return_date,
            'rent_status' => $request->rent_status
        ]);

        return redirect()->route('rent-data.index')->with('success', 'Rent data updated successfully!');
    }

    public function destroy(RentLogs $rentLog)
    {
        $rentLog->delete();

        return redirect()->route('rent-data.index')->with('success', 'Rent data deleted successfully!');
    }
}

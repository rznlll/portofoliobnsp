<?php

namespace App\Http\Controllers;

use App\Models\Car;
use App\Models\RentLogs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BookingController extends Controller
{
    public function create()
    {
        $user = Auth::user();

        // Cek apakah user memiliki rental yang sedang berjalan
        $ongoingRent = RentLogs::where('user_id', $user->id)
            ->whereIn('rent_status', ['in_process', 'approved'])
            ->first();

        if ($ongoingRent) {
            return redirect()->route('booking.status')->with('error', 'Your car booking status is being processed.');
        }

        // Dapatkan mobil yang tersedia
        $cars = Car::where('status', 'available')->get();

        return view('user.booking_create', compact('cars'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'car_id' => 'required|exists:cars,id',
            'return_date' => 'required|date|after:today',
        ]);

        $user = Auth::user();

        RentLogs::create([
            'user_id' => $user->id,
            'car_id' => $request->car_id,
            'rent_date' => now(),
            'return_date' => $request->return_date,
            'rent_status' => 'in_process',
        ]);

        return redirect()->route('booking.status')->with('success', 'Booking was submitted.');
    }

    public function status()
    {
        $user = Auth::user();
        $rentLog = RentLogs::where('user_id', $user->id)
            ->orderBy('created_at', 'desc')
            ->first();

        return view('user.booking_status', compact('rentLog'));
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Car;
use App\Models\Category;
use Illuminate\Http\Request;

class CarListController extends Controller
{
    public function index(Request $request)
    {
        $categories = Category::all();
        $query = Car::query();
    
        // Filter berdasarkan kategori
        if ($request->category) {
            $query->whereHas('categories', function ($q) use ($request) {
                $q->where('categories.id', $request->category);
            });
        }
    
        // Filter berdasarkan merek (brand)
        if ($request->brand) {
            $query->where('brand', 'like', '%' . $request->brand . '%');
        }
    
        // Ambil hasil query
        $cars = $query->get();
    
        return view('user.car_list', ['cars' => $cars, 'categories' => $categories]);
    }    
}

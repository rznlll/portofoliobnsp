<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $users = [
            [
                'username' => 'admin1',
                'password' => Hash::make('password123'),
                'phone' => '1234567890',
                'address' => 'Admin Address 1',
                'status' => 'active',
                'role_id' => 1
            ],
            [
                'username' => 'admin2',
                'password' => Hash::make('password123'),
                'phone' => '1234567891',
                'address' => 'Admin Address 2',
                'status' => 'active',
                'role_id' => 1
            ],
            [
                'username' => 'admin3',
                'password' => Hash::make('password123'),
                'phone' => '1234567892',
                'address' => 'Admin Address 3',
                'status' => 'active',
                'role_id' => 1
            ],
            [
                'username' => 'client1',
                'password' => Hash::make('password'),
                'phone' => '0987654321',
                'address' => 'Client Address 1',
                'status' => 'active',
                'role_id' => 2
            ],
            [
                'username' => 'client2',
                'password' => Hash::make('password'),
                'phone' => '0987654322',
                'address' => 'Client Address 2',
                'status' => 'active',
                'role_id' => 2
            ],
            [
                'username' => 'client3',
                'password' => Hash::make('password'),
                'phone' => '0987654324',
                'address' => 'Client Address 4',
                'status' => 'inactive',
                'role_id' => 2
            ],
            [
                'username' => 'client4',
                'password' => Hash::make('password'),
                'phone' => '0987654324',
                'address' => 'Client Address 4',
                'status' => 'inactive',
                'role_id' => 2
            ],
        ];

        foreach ($users as $user) {
            User::create($user);
        }
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('rent_logs', function (Blueprint $table) {
            $table->enum('rent_status', ['rejected', 'approved', 'in_process', 'finished'])->after('car_id')->default('in_process');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('rent_logs', function (Blueprint $table) {
            $table->dropColumn('rent_status');
        });
    }
};
